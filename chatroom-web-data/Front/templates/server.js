const express = require('express');
const server = express();
const bodyParser = require('body-parser');
const axios = require('axios');

const port = 8555;
const apiUrl = 'http://localhost:5001';

var loggedin = "";

server.use(express.urlencoded({ extended: false }));
server.use(express.json());
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));
server.use('/static', express.static('static'));
server.use('/templates', express.static('templates'));


server.get('/', function (request, response) {
    if (loggedin == true){ 
    response.sendfile('chatroom.html');
    }
    else {
        response.sendfile('index.html');
    }
});
server.get('/chatroom', function (request, response) {
    response.sendfile('chatroom.html');
});
server.get('/settings', function (request, response) {
    response.sendfile('settings.html');
});
server.get('/connexion', function (request, response) {
    response.sendfile('connexion.html');
});
server.get('/sinscrire', function (request, response) {
    response.sendfile('sinscrire.html');
});
server.get('/erreur', function (request, response) {
    response.sendfile('erreur.html');
});
server.get('/deconnexion', function (request, response) {
    response.sendfile('deconnexion.html');
});

server.get('/utilisateurs', function (request, response) {
    response.sendfile('utilisateur.html');
});



server.post('/inscription', function (request, response) {
    axios.post(apiUrl + '/sinscrire', {
        pseudo: request.body.pseudo,
        email: request.body.email,
        password: request.body.password
    }).then(function () {
        console.log('ça marche');
        response.redirect('/chatroom')
    }).catch(function () {
        console.log('no bueno');
        response.redirect('/erreur')
    })
});

server.post('/connexion', function (request, response) {
    console.log(request.body);
    axios.post(apiUrl + '/connexion', {
        email: request.body.email,
        password: request.body.password
    }).then(function (user) {
        console.log(user)
        if (loggedin == true) {
        console.log('ça marche');
        response.redirect('/chatroom')}
        else {
        console.log('compte non existant');
        response.redirect('/erreur')
        }
    })
    .catch(function () {
        console.log('no bueno');
        response.redirect('/erreur')
    })
});

server.post('/chat', function (request, response) {
    console.log(request.body);

    axios.post(apiUrl + '/chatroom', {
        pseudo1: request.body.pseudo1,
        message: request.body.message,
    }).then(function () {
        console.log('ça marche');
        response.redirect('/chatroom')
    }).catch(function () {
        console.log('no bueno');
        response.redirect('/erreur')
    })
});

server.post('/editpseudo', function (request, response) {
    console.log(request.body);

    axios.post(apiUrl + '/settings', request.body).then(function () {
        response.redirect('/settings')
        console.log('pseudo édité');
    }).catch(function () {
        response.redirect('/erreur')
        console.log('pseudo non-edité');
    })

});

server.post('/editemail', function (request, response) {
    console.log(request.body);

    axios.post(apiUrl + '/settings', request.body).then(function () {
        response.redirect('/settings')
        console.log('email édité');
    }).catch(function () {
        response.redirect('/erreur')
        console.log('email non-édité');
    })

});

server.post('/editmdp', function (request, response) {
    console.log(request.body);

    axios.post(apiUrl + '/settings', request.body).then(function () {
        response.redirect('/settings')
        console.log('mdp édité');
    }).catch(function () {
        response.redirect('/erreur')
        console.log('mdp non-édité');
    })
});



server.use(function (request, response, next) {
    response.setHeader('Content-Type', 'text/plain; charset=UTF-8');
    response.status(404).send('Page introuvable');
})

server.listen(port, function () {
    console.log('Le serveur fonctionne sur le port ' + port);
});