CREATE DATABASE chat_room;
USE chat_room;

CREATE TABLE users (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    user_pseudo VARCHAR (40) NOT NULL,
    user_password VARCHAR (15) NOT NULL,
    user_email VARCHAR (50) NOT NULL,
    creation_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE messages (
    message_id INT AUTO_INCREMENT  PRIMARY KEY,
    user_id INT NOT NULL,
    message_contenu VARCHAR (255) NOT NULL,
    message_date DATETIME NOT NULL,
    FOREIGN KEY fk_user_messages(user_id)
    REFERENCES users(user_id)
);

ALTER TABLE users
ADD UNIQUE INDEX user_pseudo_unique (user_pseudo ASC) ;

ALTER TABLE users
ADD UNIQUE INDEX  user_email_unique (user_email ASC) ;